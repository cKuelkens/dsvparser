obj1 = ''
obj2 = ''

open('noDuplicates.json', "w")

with open("output.json", "r") as fileobj:
    for line in fileobj:
        if line == '    {\n':
            obj2 = obj1
            obj1 = ''
        if line == '    },\n':
            if obj1 != obj2:
                open('noDuplicates.json', "a").write('{\n')
                open('noDuplicates.json', "a").write(obj1)
                open('noDuplicates.json', "a").write('},\n')
        if line != '    {\n' and line != '    },\n' and line != '[\n' and line != ']\n':
            obj1 = obj1 + line


