import re, sys, json, string

minutes = ''
seconds = ''
miliseconds = ''
day = ''
month = ''
year = ''
distance = ""
points = ""
pool = ""
swim_time = []
date = []
jsonFile = ''
filename = "out.json"
lane = ""
addition = ''
time = ''

def reset_fields():
    minutes = ''
    seconds = ''
    miliseconds = ''
    day = '-----'
    month = '-----'
    year = '-----'
    distance = ""
    points = ""
    pool = ""
    swim_time = []
    date = []
    addition = ''
    time = ''

variable = ""
verein = "Bocholter WSV 1920"
verein2 = "1 Weseler SV 1914"
verein3 = "Bezirk Ruhrgebiet"

for i in range(2007, 2019):
    

    variable = open(str(i) + ".html", "r").read()

    variable = re.sub('[^A-Za-z0-9]+', ' ', variable)

    #print(variable)
    variable = re.findall('tbody(.*?)tbody', variable)
    #print variable

    twentyfive = variable[0]
    fifty = variable[1]

    #print(twentyfive)
    #print("--------------------------\n")
    #print(fifty)

    twentyfive = ''.join(twentyfive)
    twentyfive = re.sub('tr ', '--- \n', twentyfive)
   

    twentyfive = re.sub('td ', '\n', twentyfive)
    twentyfive = re.sub('nobr ', '', twentyfive)
    twentyfive = re.sub('nbsp', '', twentyfive)
    twentyfive = re.sub('a href title (.*?) a', '\n', twentyfive)
    twentyfive = re.sub('a href https dsvdaten dsv de Modules Results Meet aspx MeetID (.*?)' + verein + ' ', '', twentyfive)
    twentyfive = re.sub('a href https dsvdaten dsv de Modules Results Meet aspx MeetID (.*?)' + verein2 + ' ', '', twentyfive)
    twentyfive = re.sub('a href https dsvdaten dsv de Modules Results Meet aspx MeetID (.*?)' + verein3 + ' ', '', twentyfive)
    twentyfive = re.sub(' a', '', twentyfive)
    twentyfive = re.sub('span title ', '', twentyfive)
    twentyfive = re.sub(' span', '', twentyfive)
    #twentyfive = re.sub('\n', '', twentyfive)
    #print(twentyfive)
    #open(filename, "a").write(twentyfive)

    #print (variable)

    linecount = 0
    datablock = 0
    gesamt = 0

    for line in twentyfive.splitlines():
        if linecount == 10:
            if line == '' or line == ' ' or line == '\n':
                linecount = 9
                        
            pool = line
            #print(pool)
        

        if linecount == 2:
            two = line.split()
            distance = two[0]
            try:
                addition = two[1]
            except:
                addition = 'none'

            #print (addition)


        if linecount == 4:
            four = line.split()
            try:
                minutes = four[0]
                seconds = four[1]
                miliseconds = four[2]
            except:
                addition = line
                minutes = ''
                seconds = ''
                miliseconds = ''
                
            
            #print(minutes + seconds + miliseconds)

        if linecount == 6 and addition != 'NA ' and addition != 'AB ' and addition != 'DS ':
            
            points = line

            print(points)

        

        if linecount == 12:
            twelve = line.split()
            day = twelve[0]
            month = twelve[1]
            year = twelve[2]

            date = year + '/' + month + '/' + day
            lane = '25m'

            time = minutes + ':' + seconds + ',' + miliseconds
            if addition == 'NA ' or addition == 'AB ' or addition == 'DS ':
                time = ''
                points = ''

            #print(day + month + year)

        linecount = linecount + 1
        if linecount == 14:
            linecount = 0

            jsonObject = {"distance": distance,
                    "addition": addition,
                    "lane": lane,
                    "minutes": minutes,
                    "seconds": seconds,
                    "miliseconds": miliseconds,
                    "date": date,
                    "time": time,
                    "points": points,
                    "pool": pool,
                    "day": day,
                    "month" : month,
                    "year" : year,
                    }
            jsonFile = json.dumps(jsonObject)
            outputfile = open(filename, "a").write(jsonFile)
            outputfile = open(filename, "a").write(',')

            reset_fields()





#################################Ab hier 50m#########################################




    fifty = ''.join(fifty)
    fifty = re.sub('tr ', '--- \n', fifty)
   

    fifty = re.sub('td ', '\n', fifty)
    fifty = re.sub('nobr ', '', fifty)
    fifty = re.sub('nbsp', '', fifty)
    fifty = re.sub('a href title (.*?) a', '\n', fifty)
    fifty = re.sub('a href https dsvdaten dsv de Modules Results Meet aspx MeetID (.*?)' + verein + ' ', '', fifty)
    fifty = re.sub('a href https dsvdaten dsv de Modules Results Meet aspx MeetID (.*?)' + verein2 + ' ', '', fifty)
    fifty = re.sub('a href https dsvdaten dsv de Modules Results Meet aspx MeetID (.*?)' + verein3 + ' ', '', fifty)
    fifty = re.sub(' a', '', fifty)
    fifty = re.sub('span title ', '', fifty)
    fifty = re.sub(' span', '', fifty)
    #fifty = re.sub('\n', '', fifty)
    #print(fifty)
    #open(filename, "a").write(fifty)

    #print (variable)

    linecount = 0
    datablock = 0
    gesamt = 0

    for line in fifty.splitlines():
        if linecount == 10:
            if line == '' or line == ' ' or line == '\n':
                linecount = 9
                        
            pool = line
            #print(pool)
        

        if linecount == 2:
            two = line.split()
            distance = two[0]
            try:
                addition = two[1]
            except:
                addition = 'none'

            #print (addition)


        if linecount == 4:
            four = line.split()
            try:
                minutes = four[0]
                seconds = four[1]
                miliseconds = four[2]
            except:
                addition = line
                minutes = ''
                seconds = ''
                miliseconds = ''
                
            
            #print(minutes + seconds + miliseconds)

        if linecount == 6 and addition != 'NA ' and addition != 'AB ' and addition != 'DS ':
            
            points = line

            print(points)

        

        if linecount == 12:
            twelve = line.split()
            day = twelve[0]
            month = twelve[1]
            year = twelve[2]

            date = year + '/' + day + '/' + month
            lane = '50m'

            time = minutes + ':' + seconds + ',' + miliseconds
            if addition == 'NA ' or addition == 'AB ' or addition == 'DS ':
                time = ''
                points = ''

            #print(day + month + year)

        linecount = linecount + 1
        if linecount == 14:
            linecount = 0

            jsonObject = {"distance": distance,
                    "addition": addition,
                    "lane": lane,
                    "minutes": minutes,
                    "seconds": seconds,
                    "miliseconds": miliseconds,
                    "date": date,
                    "time": time,
                    "points": points,
                    "pool": pool,
                    "day": day,
                    "month" : month,
                    "year" : year,
                    }
            jsonFile = json.dumps(jsonObject)
            outputfile = open(filename, "a").write(jsonFile)
            outputfile = open(filename, "a").write(',')

            reset_fields()



        

            

