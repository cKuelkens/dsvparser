import re, sys, json, string

minutes = ''
seconds = ''
miliseconds = ''
day = ''
month = ''
year = ''
distance = ""
points = ""
pool = ""
swim_time = []
date = []
jsonFile = ''
filename = "out.json"
lane = ""
addition = ''
time = ''

def reset_fields():
    minutes = ''
    seconds = ''
    miliseconds = ''
    day = '-----'
    month = '-----'
    year = '-----'
    distance = ""
    points = ""
    pool = ""
    swim_time = []
    date = []
    addition = ''
    time = ''

inputString = ""
verein = "Bocholter WSV 1920"
verein2 = "1 Weseler SV 1914"
verein3 = "Bezirk Ruhrgebiet"

for i in range(2007, 2019):
    

    inputString = open(str(i) + ".html", "r").read()

    inputString = re.sub('[^A-Za-z0-9]+', ' ', inputString)

    inputString = re.findall('tbody(.*?)tbody', inputString)

    dataString = ''.join(dataString)
    
    dataString = re.sub('tr ', '--- \n', dataString)
    dataString = re.sub('td ', '\n', dataString)
    dataString = re.sub('nobr ', '', dataString)
    dataString = re.sub('nbsp', '', dataString)
    dataString = re.sub('a href title (.*?) a', '\n', dataString)
    dataString = re.sub('a href https dsvdaten dsv de Modules Results Meet aspx MeetID (.*?)' + verein + ' ', '', dataString)
    dataString = re.sub(' a', '', dataString)
    dataString = re.sub('span title ', '', dataString)
    dataString = re.sub(' span', '', dataString)





    linecount = 0
    datablock = 0
    gesamt = 0

    for line in dataString.splitlines():
        if linecount == 10:
            if line == '' or line == ' ' or line == '\n':
                linecount = 9
                        
            pool = line
            #print(pool)
        

        if linecount == 2:
            two = line.split()
            distance = two[0]
            try:
                addition = two[1]
            except:
                addition = 'none'

            #print (addition)


        if linecount == 4:
            four = line.split()
            try:
                minutes = four[0]
                seconds = four[1]
                miliseconds = four[2]
            except:
                addition = line
                minutes = ''
                seconds = ''
                miliseconds = ''
                
            
            #print(minutes + seconds + miliseconds)

        if linecount == 6 and addition != 'NA ' and addition != 'AB ' and addition != 'DS ':
            
            points = line

            print(points)

        

        if linecount == 12:
            twelve = line.split()
            day = twelve[0]
            month = twelve[1]
            year = twelve[2]

            date = year + '/' + day + '/' + month
            lane = '25m'

            time = minutes + ':' + seconds + ',' + miliseconds
            if addition == 'NA ' or addition == 'AB ' or addition == 'DS ':
                time = ''
                points = ''

            #print(day + month + year)

        linecount = linecount + 1
        if linecount == 14:
            linecount = 0

            jsonObject = {"distance": distance,
                    "addition": addition,
                    "lane": lane,
                    "minutes": minutes,
                    "seconds": seconds,
                    "miliseconds": miliseconds,
                    "date": date,
                    "time": time,
                    "points": points,
                    "pool": pool,
                    "day": day,
                    "month" : month,
                    "year" : year,
                    }
            jsonFile = json.dumps(jsonObject)
            outputfile = open(filename, "a").write(jsonFile)
            outputfile = open(filename, "a").write(',')

            reset_fields()

  

