import re, sys, json, string

minutes = ''
seconds = ''
miliseconds = ''
day = '-----'
month = '-----'
year = '-----'
distance = ""
points = ""
pool = ""
swim_time = []
date = []
jsonFile = ''
filename = "output.json"
lane = "25m"
addition = ''
time = ''

def reset_fields():
    minutes = ''
    seconds = ''
    miliseconds = ''
    day = '-----'
    month = '-----'
    year = '-----'
    distance = ""
    points = ""
    pool = ""
    swim_time = []
    date = []
    addition = ''
    time = ''


open(filename, "a").write('[\n')



variable = ""
verein = "Bocholter WSV 1920"
verein2 = "1 Weseler SV 1914"
verein3 = "Bezirk Ruhrgebiet"

for i in range(2007, 2019):
    

    variable = open(str(i) + ".html", "r").read()

    variable = re.sub('[^A-Za-z0-9]+', ' ', variable)

    #print(variable)
    variable = re.findall('tbody(.*?)tbody', variable)
    #print variable
    variable = ''.join(variable)

    
 
    variable = re.sub('tr ', '-----\n', variable)
    variable = re.sub('td ', '\n', variable)
    variable = re.sub('nbsp ', '', variable)
    variable = re.sub('nbsp ', '', variable)
    variable = re.sub('nobr ', '', variable)
    variable = re.sub('span ', '', variable)
    variable = re.sub('title ', '', variable)


    variable = re.sub('a href (.*?) a', '', variable)

    variable = re.sub('spx (.*?) ' + verein, '', variable)

    variable = re.sub('spx (.*?) ' + verein2, '', variable)

    variable = re.sub('spx (.*?) ' + verein3, '', variable)

    variable = re.sub(' a ', '', variable)

    variable = re.sub('\n ', '', variable)

    variable = re.sub(' ', '', variable, 1)


    print(variable)


    ss = 0
    obj = 0


    for line in variable.splitlines():
        if line == '-----' or line == '----------':
            
            
                
                
            if ss == 1:
                ss = 0
                obj = 0
                if month != '-----':

                    if minutes != 'NA' and minutes != 'DS' and minutes != 'AB':
                        minutes = int(minutes)
                        seconds = int(seconds)
                        miliseconds = int(miliseconds)
                      

                    try:
                        points = int(points)
                    except:
                        reset_fields()
                        continue
                

                    date = str(year) + "/" + str(day) + "/" + str(month)
                    
                    time = str(minutes) + ":" + str(seconds) + ":" + str(miliseconds)

                    year = int(year)
                    month = int(month)
                    day = int(day)

                    jsonObject = {"distance": distance,
                    "addition": addition,
                    "lane": lane,
                    "minutes": minutes,
                    "seconds": seconds,
                    "miliseconds": miliseconds,
                    "date": date,
                    "time": time,
                    "points": points,
                    "pool": pool,
                    "day": day,
                    "month" : month,
                    "year" : year,
                    }
                    jsonFile = json.dumps(jsonObject)
                    outputfile = open(filename, "a").write(jsonFile)
                    outputfile = open(filename, "a").write(',')

                    reset_fields()
                    if line == '----------':
                        lane = "50m"


                    #print(jsonObject)
                    
            if ss == 0:
                ss = 1    


        elif line != '\n' and line != '' and line != '-----':
            obj = obj+1
            if obj == 1:
                splited_distance = line.split()
                distance = splited_distance[0]
                try:
                    addition = splited_distance[1]
                except:
                    addition = ''

            if obj == 2 and line != 'NA ':
                swim_time = line.split()
                
                minutes = swim_time[0]
                try:
                    seconds = swim_time[1]
                except:
                    seconds = 'Fehler'

                try:
                    miliseconds = swim_time[2]
                except:
                    miliseconds = 'Fehler'

            if obj == 2 and line == 'NA ':
                obj = 3
                minutes = 'NA'
                seconds = 'NA'
                miliseconds = 'NA'

            if obj == 2 and line == 'DS ':
                obj = 3
                minutes = 'DS'
                seconds = 'DS'
                miliseconds = 'DS'

            if obj == 2 and line == 'AB ':
                obj = 3
                minutes = 'AB'
                seconds = 'AB'
                miliseconds = 'AB'

          
            if obj == 3:
                points = line
            if obj == 4:
                pool = line
            if obj == 5:
                date = line.split()
                try:
                    day = date[0]
                except:
                    day = '-----'
                    print('Fehler im Datum')
                
                try:
                    month = date[1]
                except:
                    month = '-----'
                
                try:
                    year = date[2]
                except:
                    year = '-----'

                #print(day + '.' + month + '.' + year)
    lane = "25m"        

open(filename, "a").write('\n]')
#print(jsonFile)

#outputfile = open(filename, "w").write(jsonFile)